#!/usr/bin/python3

def boas_vindas(nome=''):
    if nome != '':
        return 'Seja bem vindo {}'.format(nome)
    else:
        return 'Nome não informado...'

print(boas_vindas())
print(boas_vindas('Lucas'))