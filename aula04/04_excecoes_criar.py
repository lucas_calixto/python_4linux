#!/usr/bin/python3

ling = input('Qual lingaugem de programação? ')

try:
    if ling.lower().strip() == 'python':
        print('Voce acertou!')
    else:
        raise ValueError('Linguagem errada')
except ValueError as e:
    print("ERRO: {}".format(e))
    