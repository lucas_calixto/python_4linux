#!/usr/bin/python3

#https://doc.sqlalchemy.org/en/13/orm/tutorial.html

from classes.Entidades import Paciente
from classes.Entidades import Funcionario
from classes.Entidades import Consulta
from config.banco import paciente_table, consulta_table, funcionario_table
from datetime import date
from config.banco import session
from sqlalchemy import select
import os

def Cadastrar_Paciente(nome_cad, ano, mes, dia):
    try:
        paciente = Paciente(
            nome = nome_cad,
            dtnasc = date(ano,mes,dia)
        )
        session.add(paciente)
        session.commit()
        return 'Usuario cadastrado com sucesso'
    except Exception as e:
        session.rollback()
        print('Erro: {}'.format(e))
    
def Marcar_Consulta(pac_id, rec_id, med_id, datac):
    if Checar_Disponibilidade(med_id, datac) == True:
        try:
            consulta = Consulta(
                paciente_id = pac_id,
                recepcionista_id = rec_id,
                medico_id = med_id,
                data = datac
            )
            session.add(consulta)
            session.commit()
            return 'Consulta Realizada com sucesso'
        except Exception as e:
            session.rollback()
            print('Erro: {}'.format(e))    
    else:
        return 'Horario indisponível'

def Checar_Disponibilidade(med_id, datac):
    try:
        result = select([funcionario_table]).where(funcionario_table.c.id == med_id)
        result.execute()
        try:
            result = select([consulta_table]).where(consulta_table.c.data == datac)
            result.execute()
            print('era pra dar exception aqui')
        except Exception as e:
            print("passou aqui")
            return True

    except Exception as e:
        print('Erro: {}'.format(e))
        return False

infinito = 1

while infinito:
    print('Selecione umas das opções abaixo: ')
    print('1. Cadastrar paciente ')
    print('2. Marcar consulta ')
    print('3. Sair ')
    opc = int(input('Digite a opção: '))
    
    if opc == 1:
        nome = str(input('Digite o nome do paciente: '))
        dia = int(input('Digite o dia de nascimento do paciente: '))
        mes = int(input('Digite o mês de nascimento do paciente: '))
        ano = int(input('Digite o ano de nascimento do paciente: '))
        # os.system('clear')
        print(Cadastrar_Paciente(nome, dia, mes, ano))

    if opc == 2:
        # paciente_id = input('Digite o id do paciente: ')
        # recepcionista_id = input('Digite o id do recepcionista: ')
        # medico_id = input('Digite o id do medico: ')
        # data_consulta = input('Digite a data da consulta')
        # os.system('clear')
        # print(Marcar_Consulta(paciente_id, recepcionista_id, medico_id, data_consulta))
        result = select([funcionario_table]).where(funcionario_table.c.id == 999)
        for usuario in result.execute():
            print (usuario)
        result.execute()
        result = select([consulta_table]).where(consulta_table.c.data == '1929-02-03 01:01:02')
        result.execute()


        # result = select([consulta_table]).where(consulta_table.c.data == data_consulta)
        # for usuario in result.execute():
        #     nome = usuario[4]
        # print (nome)
        

    if opc == 3:
        exit()


# recepcionista = Funcionario(
#     nome = 'Marcelo Dantas',
#     cargo = 'Recepcionista',
#     dtnasc = date(1978,3,5)
# )

# medico = Funcionario(
#     nome = 'Marielle Danjou',
#     cargo = 'Médico',
#     dtnasc = date(1978, 3, 5)
# )

# session.add_all([
#     paciente,
#     recepcionista,
#     medico
# ])


# # session.add(paciente)

# session.commit()
# session.rollback()

# query = session.query(Paciente)

# pacientes = query.all()

# for paciente in pacientes:
#     print(
#         paciente.id,
#         paciente.nome,
#         paciente.dtnasc
#     )

# for paciente in session.query(Paciente).filter(
#     Paciente.nome.like('Joaquim%')
# ):
#     print(
#         paciente.id,
#         paciente.nome,
#         paciente.dtnasc
#     )

# paciente = session.query(Paciente).filter(
#     Paciente.nome.like('Joaquim%')
# ).first()

# print(
#     paciente.id,
#     paciente.nome,
#     paciente.dtnasc
# )

# paciente.nome = 'Joaquim da silva'
# paciente.nasc = '1989-05-13'

# session.commit()

# paciente = session.query(Paciente).filter(
#     Paciente.nome.like('Joaquim%')
# ).first()

# print(paciente.id, paciente.nome, paciente.dtnasc)

# session.delete(paciente)
# session.commit()

# print(
#     session.query(Paciente).filter(
#         Paciente.nome.like('Joaquim%')
#     ).count()
# )