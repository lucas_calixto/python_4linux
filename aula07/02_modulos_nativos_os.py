#!/usr/bin/python3

import os

print("----------------------------------")

# Método getlogin() retorna os usuarios logado no sitemas operacional
# Não funciona em algumas distribuições linux por não conter variável
# de ambiente necessária
# Digite env no terminal para verificar variáveis de ambiente
# print('Usuário logado no sitemas', os.getlogin(), sep=':')

# Método getpass.getuser() retorna o usuario logado no
# sistema operacional
import getpass

print('Usuário logado no sistema', getpass.getuser(), sep=':')

print('----------------------------------------------------------')

# Método listdir(caminho='.') retorna um objeto list com a
# lista de diretórios e arquivos no caminho informado,
# se o parametro caminho não for informado retornará o list do
# diretório atual(do script em execução)
print('Listar Diretório', os.listdir(), sep=': ')
print('Listar Diretório', os.listdir('/home/developer'), sep=': ')

print('----------------------------------------------------------')

#criando diretórios
print('Criar diretório', os.mkdir('pythondir'), sep=': ')
print('Criar diretório', os.mkdir('pythondir2'), sep=': ')

#Gravando um arquivo
with open('pythondir2/testemodulo.txt', 'w') as arquivo:
    arquivo.write('teste')

#Renomeando diretório
print('Renomear o diretório', os.rename('pythondir', 'pythonrenomeado'), sep=': ')


#renomeando arquivo
print('Renomear arquivo', os.rename('pythondir2/testemodulo.txt', 'pythondir2/renomemodulo.txt'))

print('-------------------------------------------------')

os.system('sudo apt update')

print('---------------------------------------------')
