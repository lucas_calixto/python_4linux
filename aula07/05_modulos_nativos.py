#!/usr/bin/python3

#JSON - javascript object notation
import json

pessoa = [
    {
        'nome' : 'Philadelho',
        'dtnasc': '1937-0612'
    },
    {
        'nome' : 'Philomena',
        'dtnasc' : '1928-07-15'
    }
]

print('---------------------------------')

strJson = json.dumps(pessoa)

print('list[dict]<=>json')
print(type(strJson))
print(strJson)

print('---------------------------------')

dicionario = json.loads(strJson)

print('json<=>list[dict]')
print(type(dicionario))
print(dicionario)

print('---------------------------------')
