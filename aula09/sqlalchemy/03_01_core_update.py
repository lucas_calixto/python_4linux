#!/usr/bin/python3

from core import user_table, engine
from sqlalchemy import update

con = engine.connect()
atualizar = update(user_table).where(user_table.c.nome == 'João')

commit = atualizar.values(nome='Daniel Prata')
result = con.execute(commit)
print(result.rowcount)